use std::vec;
use std::fs::File;
use std::io::Write;
use std::io::{BufRead, BufReader};

#[allow(dead_code)]
#[derive(Debug, Copy, Clone)]
enum Orientation {
    Nord,
    Sud,
    Est,
    Ouest,
}

#[allow(dead_code)]
#[derive(Debug, Copy, Clone)]
enum Instruction {
    D,
    G,
    A
}

#[allow(dead_code)]
#[derive(Debug, Copy, Clone)]
struct Point {
    x: i32,
    y: i32,
}

struct Carte {
    superieur_gauche: Point,
    inferieur_droit: Point,
    robots : Vec < Robot >,
}
impl Carte {
    fn ajouter(mut self, robot: Robot){
        self.robots.push(robot);
    }
    
 /*   fn dans_la_carte (i32)-> i32 {
        if
    }*/
}


#[derive(Debug)]
struct Robot{
    nom: String,
    position: Point,
    orientation: Orientation,
    instructions: Vec < Instruction >, 
}
impl Robot {
    fn bouge(&mut self, instruction: Instruction){
        use crate::Orientation::*; 
        
        match instruction {
            Instruction::D =>
                match self.orientation {
                    Nord => self.orientation = Est,
                    Sud => self.orientation = Ouest,
                    Est => self.orientation = Sud,
                    Ouest => self.orientation = Nord,
                }
            Instruction::G =>
                match self.orientation {
                    Nord => self.orientation = Ouest,
                    Sud => self.orientation = Est,
                    Est => self.orientation = Nord,
                    Ouest => self.orientation = Sud,
                }
            Instruction::A => 
                match self.orientation {
                    Nord => {self.position.y = self.position.y - 1;}
                    Sud => {self.position.y = self.position.y + 1;}
                    Est => {self.position.x = self.position.x + 1;}
                    Ouest => {self.position.x = self.position.x - 1;}
                } 
        }
    }

    fn affiche(&self){
        print!("le robot est en {}, {} ", self.position.x, self.position.y);
        match self.orientation {
            Nord => {println!("orienté vers le nord.");}
            Sud => {println!("orienté vers le sud.");}
            Est => {println!("orienté vers le est.");}
            Ouest => {println!("orienté vers le ouest.");}
        }
    }
}


fn main(){
    use crate::Instruction::*;
    
    let _carte = Carte {
        superieur_gauche: Point { x:0, y:0 }, 
        inferieur_droit: Point {x:10, y:10},
        robots : Vec::new(),
    };

    let mut point1 = Point {x:0, y:0};
    let mut robot1 = Robot{
        nom: String::from("robot1"),
        position: point1,
        orientation: Orientation::Nord,
        instructions: vec![D,G], 
    };
    let instructions = vec![D,G];
    robot1.bouge(D);
    robot1.affiche();

    robot1.bouge(G);
    robot1.affiche();
    
    robot1.bouge(A);
    robot1.affiche();

}
